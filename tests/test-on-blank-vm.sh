#! /bin/bash

set -ex

dpkg -l vagrant > /dev/null

cd role-test-vm

vagrant destroy
vagrant up

vagrant ssh -c 'echo "- hosts: all" > test.yml'
vagrant ssh -c 'echo "  become_method: sudo" >> test.yml'
vagrant ssh -c 'echo "  tasks:" >> test.yml'
vagrant ssh -c 'echo "   - include_role: name=ansible-role-install-fdroidserver-dependencies" >> test.yml'
vagrant ssh -c 'echo "     become: yes" >> test.yml'

vagrant ssh -c 'ansible-playbook -i localhost, -c local test.yml'
